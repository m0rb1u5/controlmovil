package com.microchip.controlmovil;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class DispositivosActivity extends Activity {
    private ListView listView;
    private ArrayAdapter adaptador;
    private BluetoothAdapter bluetoothAdapter;
    public static String ADDRESS = "Address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispositivos);

        listView = (ListView) findViewById(R.id.listView);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter==null)
            Toast.makeText(getBaseContext(), "El dispositivo no soporta Bluetooth", Toast.LENGTH_LONG).show();
        else if (bluetoothAdapter.isEnabled())
            Toast.makeText(getBaseContext(), "Bluetooth activado", Toast.LENGTH_LONG).show();
        else {
            Intent permitirBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(permitirBluetooth);
        }

        adaptador = new ArrayAdapter(this, R.layout.dispositivo);
        listView.setAdapter(adaptador);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), "Conectando...", Toast.LENGTH_LONG).show();
                String informacion = ((TextView) view).getText().toString();
                String address = informacion.substring(informacion.length() - 17);

                Intent intent = new Intent(DispositivosActivity.this, MainActivity.class);
                intent.putExtra(ADDRESS, address);
                startActivity(intent);
            }
        });

        Set<BluetoothDevice> dispositivosUnidos = bluetoothAdapter.getBondedDevices();

        if ( dispositivosUnidos.size() > 0 ) {
            for (BluetoothDevice dispositivo : dispositivosUnidos) {
                adaptador.add(dispositivo.getName() + "\n" + dispositivo.getAddress() );
            }
        }
        else {
            String ningun = "No hay dispositivos bloutooth";
            adaptador.add(ningun);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dispositivos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
