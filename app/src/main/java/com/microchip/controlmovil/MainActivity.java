package com.microchip.controlmovil;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import android.os.Handler;
import java.util.logging.LogRecord;

public class MainActivity extends Activity {
    private ImageButton button_flecha_arriba;
    private ImageButton button_flecha_izquierda;
    private ImageButton button_parar;
    private ImageButton button_flecha_derecha;
    private ImageButton button_flecha_abajo;
    private Button button_on;
    private Button button_off;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket bluetoothSocket;
    private HilosConectados hilo;
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_flecha_arriba = (ImageButton) findViewById(R.id.button_flecha_arriba);
        button_flecha_izquierda = (ImageButton) findViewById(R.id.button_flecha_izquierda);
        button_flecha_derecha = (ImageButton) findViewById(R.id.button_flecha_derecha);
        button_flecha_abajo = (ImageButton) findViewById(R.id.button_flecha_abajo);
        button_parar = (ImageButton) findViewById(R.id.button_parar);
        button_on = (Button) findViewById(R.id.button_on);
        button_off = (Button) findViewById(R.id.button_off);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter==null)
            Toast.makeText(getBaseContext(), "El dispositivo no soporta Bluetooth", Toast.LENGTH_LONG).show();
        else if (bluetoothAdapter.isEnabled())
            Toast.makeText(getBaseContext(), "Bluetooth activado", Toast.LENGTH_LONG).show();
        else {
            Intent permitirBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(permitirBluetooth);
        }

        button_flecha_arriba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("a");
            }
        });
        button_flecha_izquierda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("b");
            }
        });
        button_flecha_derecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("d");
            }
        });
        button_flecha_abajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("e");
            }
        });
        button_parar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("c");
                Toast.makeText(getBaseContext(), "Se ha parado el móvil", Toast.LENGTH_LONG);
            }
        });

        button_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("f");
                button_flecha_arriba.setEnabled(false);
                button_flecha_izquierda.setEnabled(false);
                button_flecha_derecha.setEnabled(false);
                button_flecha_abajo.setEnabled(false);
                button_parar.setEnabled(false);
                Toast.makeText(getBaseContext(), "Sistema inteligente activado.", Toast.LENGTH_LONG);
            }
        });

        button_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hilo.write("g");
                button_flecha_arriba.setEnabled(true);
                button_flecha_izquierda.setEnabled(true);
                button_flecha_derecha.setEnabled(true);
                button_flecha_abajo.setEnabled(true);
                button_parar.setEnabled(true);
                Toast.makeText(getBaseContext(), "Sistema inteligente desactivado.", Toast.LENGTH_LONG);
            }
        });

        Intent intent = getIntent();
        address = intent.getStringExtra(DispositivosActivity.ADDRESS);

        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);
        try {
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(BTMODULEUUID);
        }
        catch ( IOException error ) {
            Toast.makeText(getBaseContext(), "La conección al dispositivo falló", Toast.LENGTH_LONG ).show();
        }
        try {
            bluetoothSocket.connect();
        }
        catch ( IOException error ) {
            try {
                bluetoothSocket.close();
            }
            catch ( IOException error2 ) {
                Log.e("Error:",error2.toString());
            }
        }
        hilo = new HilosConectados(bluetoothSocket);
        hilo.start();
        hilo.write("X");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause(){
        super.onPause();
        try {
            bluetoothSocket.close();
        }
        catch ( IOException error ) {
            Log.e("Error", error.toString());
        }
    }

    private class HilosConectados extends Thread {
        private final OutputStream mmOutStream;

        public HilosConectados(BluetoothSocket socket) {
            OutputStream tmpOut = null;

            try {
                tmpOut = socket.getOutputStream();
            }
            catch (IOException e) {
                Log.e("Error:",e.toString());
            }

            mmOutStream = tmpOut;
        }


        public void run() {
        }
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            }
            catch (IOException e) {
                Toast.makeText(getBaseContext(), "La Conexión falló", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, DispositivosActivity.class);
                startActivity(intent);
            }
        }
    }
}
